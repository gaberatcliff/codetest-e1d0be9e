var mongoose = require('mongoose');
var Task = mongoose.model('Task');

module.exports = (function (){
	return {
		show: function (req, res){
			Task.find({}, function (err, tasks){
				if (err){
					console.log(err);
				}
				else{
					res.json(tasks);
				}
			})
		},
		create: function (req, res){
			Task.find({}, function (err, tasks){
				if (err){
					console.log(err);
				}else{
					var task = new Task({description: req.body.description, completed: req.body.completed, order: tasks.length + 1});
					task.save(function (err, newTask){
						if (err){
							console.log(err);
						}else{
							res.json(newTask);
						}
					})
				}
			})
		},
		remove: function (req, res){
			console.log(req.body);
			Task.remove({_id: req.body._id}, function (err, tasks){
				if (err){
					console.log(err);
				} else{
					res.json('removed');
				}
			})
		},
		complete: function (req, res){
			Task.findOneAndUpdate({_id: req.body._id}, {completed: "completed"}, function (err, results){
				if (err){
					console.log("cannot complete task");
				}else {
					console.log('completed.');
					res.json(results);
				}
			})
		},
		update: function (req, res){
			Task.findOneAndUpdate({_id: req.body._id}, {description: req.body.description}, function (err, results){
				if (err){
					console.log(err);
				}else{
					res.json(results);
				}
			})
		},
		moveUp: function (req, res){
			var current = req.body.order;
			var previous = req.body.order - 1;
			Task.findOneAndUpdate({order: previous}, {$set: {order: current}}, {new: true}, function (err, results){
				if (err){
					console.log(err);
				}else{
					Task.findOneAndUpdate({_id: req.body._id}, {$set: {order: previous}}, {new: true}, function (err, results){
						if (err){
							console.log(err);
						}else{
							res.json(results);
						}
					})
				}
			})
		},
		moveDown: function (req, res){
			var current = req.body.order;
			var next = req.body.order + 1;
			Task.findOneAndUpdate({order: next}, {$set: {order: current}}, {new: true}, function (err, results){
				if (err){
					console.log(err);
				}else{
					Task.findOneAndUpdate({_id: req.body._id}, {$set: {order: next}}, {new: true}, function (err, results){
						if (err){
							console.log(err);
						}else{
							res.json(results);
						}
					})
				}
			})
		}
	}
})();