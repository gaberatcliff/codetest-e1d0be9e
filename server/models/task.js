var mongoose = require ('mongoose');

var Schema = mongoose.Schema;
var taskSchema = new mongoose.Schema({
	description: String,
	completed: String,
	order: Number,
	created_at: {type: Date, default: new Date}
});
var Task = mongoose.model('Task', taskSchema);