var tasks = require('./../controllers/tasks.js');
module.exports = function (app){

	app.get('/tasks', function (req, res){
		tasks.show(req, res);
		console.log('getting tasks');
	})

	app.post('/tasks/create', function (req, res){
		tasks.create(req, res);
	})

	app.post('/tasks/remove', function (req, res){
		tasks.remove(req, res);
	})

	app.post('/tasks/complete', function (req, res){
		tasks.complete(req, res);
	})

	app.post('/tasks/update', function (req, res){
		tasks.update(req, res);
	})

	app.post('/tasks/moveUp', function (req, res){
		tasks.moveUp(req, res);
	})
	
	app.post('/tasks/moveDown', function (req, res){
		tasks.moveDown(req, res);
	})
}