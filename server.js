// Hello! I'm excited for you to check out my take on this tech challenge! Unfortunately, I don't have a 
// huge amount of experience Unit testing. Everything else should be in place. Thank you for the opportunity!
// Gabriel 
var express = require('express');
var path = require('path');
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, './client')));

require('./server/config/mongoose.js');

require('./server/config/routes.js')(app);
app.listen(8000, function (){
	console.log('Hey, here listening to port 8000!');
})