Credo.controller('indexController', function ($scope, indexFactory){
	$scope.tasks = [];
	$scope.selectedTask = {};
	$scope.isSelected = false;

	//populate table
	 indexFactory.index(function (data){
		$scope.tasks = data;
	})

	$scope.addTask = function (task){
		if(task){
			task.completed = 'active';
			indexFactory.create(task, function (data){
				$scope.tasks.push(data);
				$scope.newTask = {};
			})
		}
	}
	$scope.removeTask = function (task){
		 indexFactory.remove(task, function (data){
			if (data){
				$scope.tasks.splice($scope.tasks.indexOf(task), 1);
			}
		});
	}
	$scope.openEdit = function (selectedTask){
		//console.log(selectedTask);
		$scope.selectedTask = selectedTask;
		if ($scope.isSelected == true){
			$scope.isSelected = false;
		}
		else {
			$scope.isSelected = true;
		}
	}

	$scope.updateTask = function (description){
		//console.log(description);
		if (description){
			var taskUpdate = {};
			taskUpdate.description = description;
			taskUpdate._id = $scope.selectedTask._id;
			indexFactory.update(taskUpdate, function (data){
				if (data){
					$scope.isSelected = false;
					$scope.editTask = {};
					$scope.tasks[$scope.tasks.indexOf($scope.selectedTask)].description = description;
				}
			})
		}
	}

	$scope.completeTask = function (taskID){
		//console.log(taskID);	
		 indexFactory.completeTask(taskID, function(data){
			if (data){
				console.log(data);
				$scope.tasks[$scope.tasks.indexOf(taskID)].completed = "completed";
				$scope.isSelected = false;
			}
		})

	}

	$scope.moveUp = function (index){
		indexFactory.moveUp(index, function (data){
			if (data){
				indexFactory.index(function (data){
					$scope.tasks = data;
				})
			}
		})
	}

	$scope.moveDown = function (index){
		indexFactory.moveDown(index, function (data){
			if (data){
				indexFactory.index(function (data){
					$scope.tasks = data;
				})
			}
		})
	}
})