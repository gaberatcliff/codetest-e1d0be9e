Credo.factory('indexFactory', function ($http){
	var factory = {};

	factory.index = function (callback){
		$http.get('/tasks').success(function (output){
			if (output){
				callback(output);
			}
		})
	}
	factory.create = function (info, callback){
		$http.post('/tasks/create', info).success(function (output){
			if (output){
				callback(output);
			}
		})
	}
	factory.remove = function (info, callback){
		$http.post('/tasks/remove', info).success(function (output){
			if (output){
				callback(output);
			}
		})
	}
	factory.completeTask = function (info, callback){
		console.log(info);
		$http.post('/tasks/complete', info).success(function (output){
			if (output){
				callback(output);
			}
		})
	}
	factory.update = function (info, callback){
		$http.post('/tasks/update', info).success(function (output){
			if (output){
				callback(output);
			}
		})
	}
	factory.moveUp = function (info, callback){
		$http.post('/tasks/moveUp', info).success(function (output){
			if (output){
				callback(output);
			}
		})
	}
	factory.moveDown = function (info, callback){
		$http.post('/tasks/moveDown', info).success(function (output){
			if (output){
				callback(output);
			}
		})
	}
	return factory
})